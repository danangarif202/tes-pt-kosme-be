-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 04 Jun 2022 pada 07.21
-- Versi server: 10.4.8-MariaDB
-- Versi PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kosme_db`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `armada`
--

CREATE TABLE `armada` (
  `id` int(4) NOT NULL,
  `nama_armada` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `armada`
--

INSERT INTO `armada` (`id`, `nama_armada`) VALUES
(1, 'K-LOG . 001'),
(2, 'K-LOG . 002'),
(3, 'K-LOG . 003'),
(4, 'K-LOG . 004'),
(5, 'K-LOG . 005'),
(6, 'K-LOG . 006'),
(7, 'K-LOG . 008'),
(8, 'K-LOG . 009');

-- --------------------------------------------------------

--
-- Struktur dari tabel `driver`
--

CREATE TABLE `driver` (
  `id` int(4) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `driver`
--

INSERT INTO `driver` (`id`, `name`) VALUES
(1, 'Agus Hariyanto'),
(2, 'Nur Wahyudi'),
(3, 'Iskandar'),
(4, 'Itok Wahyudi'),
(5, 'Edi Widodo'),
(6, 'Hardadik');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kendaraan`
--

CREATE TABLE `kendaraan` (
  `id` int(4) NOT NULL,
  `tipe` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kendaraan`
--

INSERT INTO `kendaraan` (`id`, `tipe`) VALUES
(1, 'Wingbox Tronton'),
(2, 'Wingbox Box Besar'),
(3, 'Wingbox Box Ceper'),
(4, 'NLR'),
(5, 'NMR');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pivot`
--

CREATE TABLE `pivot` (
  `id` int(4) NOT NULL,
  `armada_id` int(4) NOT NULL,
  `kode_id` int(4) NOT NULL,
  `plat_id` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pivot`
--

INSERT INTO `pivot` (`id`, `armada_id`, `kode_id`, `plat_id`) VALUES
(1, 1, 1, 1),
(2, 2, 1, 2),
(3, 3, 2, 3),
(4, 4, 2, 4),
(5, 5, 3, 5),
(6, 6, 3, 6),
(7, 7, 5, 7),
(8, 8, 4, 8);

-- --------------------------------------------------------

--
-- Struktur dari tabel `plat`
--

CREATE TABLE `plat` (
  `id` int(4) NOT NULL,
  `plat_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `plat`
--

INSERT INTO `plat` (`id`, `plat_name`) VALUES
(1, 'N 9357 EK'),
(2, 'N 9356 EK'),
(3, 'N 9443 EJ'),
(4, 'N 8231 EH'),
(5, 'N 9163 EJ'),
(6, 'N 8141 EI'),
(7, 'N 8755 EI'),
(8, 'N 9848 ED');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tracking`
--

CREATE TABLE `tracking` (
  `id` int(4) NOT NULL,
  `armada_id` int(4) NOT NULL,
  `driver_id` int(4) NOT NULL,
  `plat_id` int(4) NOT NULL,
  `kendaraan_id` int(4) NOT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `finish_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tracking`
--

INSERT INTO `tracking` (`id`, `armada_id`, `driver_id`, `plat_id`, `kendaraan_id`, `start_time`, `finish_time`) VALUES
(1, 4, 3, 4, 2, '2022-06-03 00:21:58', '2022-06-03 00:28:52'),
(2, 3, 3, 3, 2, '2022-06-03 00:29:05', NULL),
(3, 3, 2, 3, 2, '2022-06-03 05:24:11', '2022-06-03 05:25:51'),
(4, 1, 2, 1, 1, '2022-06-03 05:27:28', '2022-06-03 05:28:10'),
(5, 1, 2, 1, 1, '2022-06-03 05:36:33', '2022-06-03 05:39:41');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `armada`
--
ALTER TABLE `armada`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `driver`
--
ALTER TABLE `driver`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kendaraan`
--
ALTER TABLE `kendaraan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pivot`
--
ALTER TABLE `pivot`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `plat`
--
ALTER TABLE `plat`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tracking`
--
ALTER TABLE `tracking`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `armada`
--
ALTER TABLE `armada`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `driver`
--
ALTER TABLE `driver`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `kendaraan`
--
ALTER TABLE `kendaraan`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `pivot`
--
ALTER TABLE `pivot`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `plat`
--
ALTER TABLE `plat`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `tracking`
--
ALTER TABLE `tracking`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
